package agricolaexpress.dices;

import android.annotation.SuppressLint;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Random;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class FullscreenActivity extends AppCompatActivity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();

    private View mBackgroundView;

    private boolean mVisible;

    private int dicesValues[] = {
            0, // farmDice_1
            0, // farmDice_2
            0, // occupationDice
            0, // improvementDice
            0, // produceDice_1
            0, // produceDice_2
            0, // produceDice_3
            0, // resourceDice_1
            0, // resourceDice_2
            0, // resourceDice_3
            0, // animalDice_1
            0, // animalDice_2
            0  // animalDice_3
    } ;

    private int historyPosition ;

    private ArrayList<int[]> historyDicesValues ;

    private ImageView mFarmDice_1 ;
    private ImageView mFarmDice_2 ;

    private ImageView mOccupationDice ;

    private ImageView mImprovementDice ;

    private ImageView mProduceDice_1 ;
    private ImageView mProduceDice_2 ;
    private ImageView mProduceDice_3 ;

    private ImageView mResourceDice_1 ;
    private ImageView mResourceDice_2 ;
    private ImageView mResourceDice_3 ;

    private ImageView mAnimalDice_1 ;
    private ImageView mAnimalDice_2 ;
    private ImageView mAnimalDice_3 ;

    private int iDiceClicked ;

    private ImageView mThrowDices ;

    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mBackgroundView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };

    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
        }
    };

    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (AUTO_HIDE) {
                        delayedHide(AUTO_HIDE_DELAY_MILLIS);
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    view.performClick();
                    break;
                default:
                    break;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fullscreen);

        historyDicesValues = new ArrayList<int[]>() ;

        mVisible = true;

        mBackgroundView = findViewById(R.id.clBackgroundView);

        mFarmDice_1 = findViewById( R.id.ivFarmDice_1) ;
        mFarmDice_2 = findViewById( R.id.ivFarmDice_2) ;

        mOccupationDice = findViewById( R.id.ivOccupationDice) ;

        mImprovementDice = findViewById( R.id.ivImprovementDice) ;

        mProduceDice_1 = findViewById( R.id.ivProduceDice_1) ;
        mProduceDice_2 = findViewById( R.id.ivProduceDice_2) ;
        mProduceDice_3 = findViewById( R.id.ivProduceDice_3) ;

        mResourceDice_1 = findViewById( R.id.ivResourceDice_1) ;
        mResourceDice_2 = findViewById( R.id.ivResourceDice_2) ;
        mResourceDice_3 = findViewById( R.id.ivResourceDice_3) ;

        mAnimalDice_1 = findViewById( R.id.ivAnimalDice_1) ;
        mAnimalDice_2 = findViewById( R.id.ivAnimalDice_2) ;
        mAnimalDice_3 = findViewById( R.id.ivAnimalDice_3) ;

        mThrowDices = findViewById( R.id.ivRefresh ) ;

        throwDices( mThrowDices );
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    private void show() {
        // Show the system bar
        mVisible = true;
    }

    /**
     * Schedules a call to hide() in delay milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    public void diceClick(View v) {

        if (historyPosition == historyDicesValues.size()) {

            int newDicesValues[] = new int[dicesValues.length];
            System.arraycopy(dicesValues, 0, newDicesValues, 0, newDicesValues.length);

            switch (v.getId()) {
                case R.id.ivFarmDice_1:
                    iDiceClicked = 0 ;
                    break;
                case R.id.ivFarmDice_2:
                    iDiceClicked = 1 ;
                    break;
                case R.id.ivOccupationDice:
                    iDiceClicked = 2 ;
                    break;
                case R.id.ivImprovementDice:
                    iDiceClicked = 3 ;
                    break;
                case R.id.ivProduceDice_1:
                    iDiceClicked = 4 ;
                    break;
                case R.id.ivProduceDice_2:
                    iDiceClicked = 5 ;
                    break;
                case R.id.ivProduceDice_3:
                    iDiceClicked = 6 ;
                    break;
                case R.id.ivResourceDice_1:
                    iDiceClicked = 7 ;
                    break;
                case R.id.ivResourceDice_2:
                    iDiceClicked = 8 ;
                    break;
                case R.id.ivResourceDice_3:
                    iDiceClicked = 9 ;
                    break;
                case R.id.ivAnimalDice_1:
                    iDiceClicked = 10 ;
                    break;
                case R.id.ivAnimalDice_2:
                    iDiceClicked = 11 ;
                    break;
                case R.id.ivAnimalDice_3:
                    iDiceClicked = 12 ;
                    break;
                default:
                    break;
            }

            newDicesValues[iDiceClicked] = ( newDicesValues[iDiceClicked] + 1 ) % 7  ;

            historyDicesValues.add(newDicesValues);
            historyPosition = historyDicesValues.size();
            System.arraycopy(newDicesValues, 0, dicesValues, 0, dicesValues.length);

            displayDices();
        }
    }

    public void throwDices(View v) {
        if( true == mVisible ) {
            hide();
        } else {

            int r = 0;
            int lastDiceValue[] = dicesValues;
            int newDicesValues[] = new int[dicesValues.length];

            for (int i = 0; i < dicesValues.length; i++) {

                boolean bThrow = true;
                while (bThrow) {
                    // Throw the dice
                    r = throwDice();

                    if (lastDiceValue[i] != r) {
                        bThrow = false;
                    }
                }
                dicesValues[i] = r;
                newDicesValues[i] = r;
            }

            historyDicesValues.add(newDicesValues);
            historyPosition = historyDicesValues.size();

            displayDices();
        }
    }

    public void undo(View v) {
        if( true == mVisible ) {
            hide();
        } else {

            if (historyPosition > 1) {
                historyPosition--;
                System.arraycopy(historyDicesValues.get(historyPosition - 1), 0, dicesValues, 0, dicesValues.length);

                displayDices();
            }
        }
    }

    public void redo(View v) {
        if( true == mVisible ) {
            hide();
        } else {
            hide();

            if (historyPosition < historyDicesValues.size()) {
                historyPosition++;
                System.arraycopy(historyDicesValues.get(historyPosition - 1), 0, dicesValues, 0, dicesValues.length);

                displayDices();
            }
        }
    }


    private int throwDice() {
        int r = 0 ;

        // Throw the dice
        r = new Random().nextInt(46655);  // [0...5]
        r = r % 6 + 1;

        return r ;
    }

    private void displayDices() {
        switch (dicesValues[0]) {
            case 0:
                mFarmDice_1.setImageResource(R.drawable.grey);
                break;
            case 1:
                mFarmDice_1.setImageResource(R.drawable.orange_1);
                break;
            case 2:
                mFarmDice_1.setImageResource(R.drawable.orange_2);
                break;
            case 3:
                mFarmDice_1.setImageResource(R.drawable.orange_3);
                break;
            case 4:
                mFarmDice_1.setImageResource(R.drawable.orange_4);
                break;
            case 5:
                mFarmDice_1.setImageResource(R.drawable.orange_5);
                break;
            case 6:
                mFarmDice_1.setImageResource(R.drawable.orange_6);
                break;
            default:
                break;
        }

        switch( dicesValues[1] ) {
            case 0:
                mFarmDice_2.setImageResource(R.drawable.grey);
                break;
            case 1:
                mFarmDice_2.setImageResource(R.drawable.orange_1);
                break ;
            case 2:
                mFarmDice_2.setImageResource(R.drawable.orange_2);
                break ;
            case 3:
                mFarmDice_2.setImageResource(R.drawable.orange_3);
                break ;
            case 4:
                mFarmDice_2.setImageResource(R.drawable.orange_4);
                break ;
            case 5:
                mFarmDice_2.setImageResource(R.drawable.orange_5);
                break ;
            case 6:
                mFarmDice_2.setImageResource(R.drawable.orange_6);
                break ;
            default :
                break ;
        }

        switch (dicesValues[2]) {
            case 0:
                mOccupationDice.setImageResource(R.drawable.grey);
                break;
            case 1:
                mOccupationDice.setImageResource(R.drawable.yellow_1);
                break;
            case 2:
                mOccupationDice.setImageResource(R.drawable.yellow_2);
                break;
            case 3:
                mOccupationDice.setImageResource(R.drawable.yellow_3);
                break;
            case 4:
                mOccupationDice.setImageResource(R.drawable.yellow_4);
                break;
            case 5:
                mOccupationDice.setImageResource(R.drawable.yellow_5);
                break;
            case 6:
                mOccupationDice.setImageResource(R.drawable.yellow_6);
                break;
            default:
                break;
        }

        switch (dicesValues[3]) {
            case 0:
                mImprovementDice.setImageResource(R.drawable.grey);
                break;
            case 1:
                mImprovementDice.setImageResource(R.drawable.red_1);
                break;
            case 2:
                mImprovementDice.setImageResource(R.drawable.red_2);
                break;
            case 3:
                mImprovementDice.setImageResource(R.drawable.red_3);
                break;
            case 4:
                mImprovementDice.setImageResource(R.drawable.red_4);
                break;
            case 5:
                mImprovementDice.setImageResource(R.drawable.red_5);
                break;
            case 6:
                mImprovementDice.setImageResource(R.drawable.red_6);
                break;
            default:
                break;
        }

        switch (dicesValues[4]) {
            case 0:
                mProduceDice_1.setImageResource(R.drawable.grey);
                break;
            case 1:
                mProduceDice_1.setImageResource(R.drawable.green_1);
                break;
            case 2:
                mProduceDice_1.setImageResource(R.drawable.green_2);
                break;
            case 3:
                mProduceDice_1.setImageResource(R.drawable.green_3);
                break;
            case 4:
                mProduceDice_1.setImageResource(R.drawable.green_4);
                break;
            case 5:
                mProduceDice_1.setImageResource(R.drawable.green_5);
                break;
            case 6:
                mProduceDice_1.setImageResource(R.drawable.green_6);
                break;
            default:
                break;
        }

        switch (dicesValues[5]) {
            case 0:
                mProduceDice_2.setImageResource(R.drawable.grey);
                break;
            case 1:
                mProduceDice_2.setImageResource(R.drawable.green_1);
                break;
            case 2:
                mProduceDice_2.setImageResource(R.drawable.green_2);
                break;
            case 3:
                mProduceDice_2.setImageResource(R.drawable.green_3);
                break;
            case 4:
                mProduceDice_2.setImageResource(R.drawable.green_4);
                break;
            case 5:
                mProduceDice_2.setImageResource(R.drawable.green_5);
                break;
            case 6:
                mProduceDice_2.setImageResource(R.drawable.green_6);
                break;
            default:
                break;
        }

        switch (dicesValues[6]) {
            case 0:
                mProduceDice_3.setImageResource(R.drawable.grey);
                break;
            case 1:
                mProduceDice_3.setImageResource(R.drawable.green_1);
                break;
            case 2:
                mProduceDice_3.setImageResource(R.drawable.green_2);
                break;
            case 3:
                mProduceDice_3.setImageResource(R.drawable.green_3);
                break;
            case 4:
                mProduceDice_3.setImageResource(R.drawable.green_4);
                break;
            case 5:
                mProduceDice_3.setImageResource(R.drawable.green_5);
                break;
            case 6:
                mProduceDice_3.setImageResource(R.drawable.green_6);
                break;
            default:
                break;
        }

        switch (dicesValues[7]) {
            case 0:
                mResourceDice_1.setImageResource(R.drawable.grey);
                break;
            case 1:
                mResourceDice_1.setImageResource(R.drawable.wood_1);
                break;
            case 2:
                mResourceDice_1.setImageResource(R.drawable.wood_2);
                break;
            case 3:
                mResourceDice_1.setImageResource(R.drawable.wood_3);
                break;
            case 4:
                mResourceDice_1.setImageResource(R.drawable.wood_4);
                break;
            case 5:
                mResourceDice_1.setImageResource(R.drawable.wood_5);
                break;
            case 6:
                mResourceDice_1.setImageResource(R.drawable.wood_6);
                break;
            default:
                break;
        }

        switch (dicesValues[8]) {
            case 0:
                mResourceDice_2.setImageResource(R.drawable.grey);
                break;
            case 1:
                mResourceDice_2.setImageResource(R.drawable.wood_1);
                break;
            case 2:
                mResourceDice_2.setImageResource(R.drawable.wood_2);
                break;
            case 3:
                mResourceDice_2.setImageResource(R.drawable.wood_3);
                break;
            case 4:
                mResourceDice_2.setImageResource(R.drawable.wood_4);
                break;
            case 5:
                mResourceDice_2.setImageResource(R.drawable.wood_5);
                break;
            case 6:
                mResourceDice_2.setImageResource(R.drawable.wood_6);
                break;
            default:
                break;
        }

        switch (dicesValues[9]) {
            case 0:
                mResourceDice_3.setImageResource(R.drawable.grey);
                break;
            case 1:
                mResourceDice_3.setImageResource(R.drawable.wood_1);
                break;
            case 2:
                mResourceDice_3.setImageResource(R.drawable.wood_2);
                break;
            case 3:
                mResourceDice_3.setImageResource(R.drawable.wood_3);
                break;
            case 4:
                mResourceDice_3.setImageResource(R.drawable.wood_4);
                break;
            case 5:
                mResourceDice_3.setImageResource(R.drawable.wood_5);
                break;
            case 6:
                mResourceDice_3.setImageResource(R.drawable.wood_6);
                break;
            default:
                break;
        }

        switch (dicesValues[10]) {
            case 0:
                mAnimalDice_1.setImageResource(R.drawable.grey);
                break;
            case 1:
                mAnimalDice_1.setImageResource(R.drawable.brown_1);
                break;
            case 2:
                mAnimalDice_1.setImageResource(R.drawable.brown_2);
                break;
            case 3:
                mAnimalDice_1.setImageResource(R.drawable.brown_3);
                break;
            case 4:
                mAnimalDice_1.setImageResource(R.drawable.brown_4);
                break;
            case 5:
                mAnimalDice_1.setImageResource(R.drawable.brown_5);
                break;
            case 6:
                mAnimalDice_1.setImageResource(R.drawable.brown_6);
                break;
            default:
                break;
        }

        switch (dicesValues[11]) {
            case 0:
                mAnimalDice_2.setImageResource(R.drawable.grey);
                break;
            case 1:
                mAnimalDice_2.setImageResource(R.drawable.brown_1);
                break;
            case 2:
                mAnimalDice_2.setImageResource(R.drawable.brown_2);
                break;
            case 3:
                mAnimalDice_2.setImageResource(R.drawable.brown_3);
                break;
            case 4:
                mAnimalDice_2.setImageResource(R.drawable.brown_4);
                break;
            case 5:
                mAnimalDice_2.setImageResource(R.drawable.brown_5);
                break;
            case 6:
                mAnimalDice_2.setImageResource(R.drawable.brown_6);
                break;
            default:
                break;
        }

        switch (dicesValues[12]) {
            case 0:
                mAnimalDice_3.setImageResource(R.drawable.grey);
                break;
            case 1:
                mAnimalDice_3.setImageResource(R.drawable.brown_1);
                break;
            case 2:
                mAnimalDice_3.setImageResource(R.drawable.brown_2);
                break;
            case 3:
                mAnimalDice_3.setImageResource(R.drawable.brown_3);
                break;
            case 4:
                mAnimalDice_3.setImageResource(R.drawable.brown_4);
                break;
            case 5:
                mAnimalDice_3.setImageResource(R.drawable.brown_5);
                break;
            case 6:
                mAnimalDice_3.setImageResource(R.drawable.brown_6);
                break;
            default:
                break;
        }

        if( historyPosition != historyDicesValues.size() ) {
            mThrowDices.setVisibility(View.INVISIBLE);
        } else {
            mThrowDices.setVisibility(View.VISIBLE);
        }
    }
}